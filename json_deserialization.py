import json
import os

glossary = None

with open(os.path.join("files", "glossary.json")) as gl_fp:
    glossary = json.load(gl_fp)

print(f'Data type of de-serialized JSON: {type(glossary)}')
print(f'Title of Glossary: {glossary["glossary"]["title"]}')
print(f'Glossary definition: {glossary["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["para"]}')
