import click
from lxml import etree
from lxml.etree import Element, SubElement

from data_models.music_store_models import Catalog, Cd


def return_test_catalogue():
    catalog = Catalog()

    catalog.add_cd(
        Cd(
            "Test Song",
            "QA Artist",
            "Australia",
            "Bubochka",
            34.5,
            2012
        )
    )

    catalog.add_cd(
        Cd(
            "QA Lied",
            "Rammstein",
            "Germany",
            "Liedmeister",
            54.2,
            2020
        )
    )

    return catalog

@click.command()
@click.option("--output", prompt="Write the path to serialize", help="Path to serialize")
def serialize_catalogue(output):
    catalog_element = Element("CATALOG")
    catalogues = return_test_catalogue()

    for cat in catalogues.cds:
        cd_inside_catalog = SubElement(catalog_element, "CD")
        title = SubElement(cd_inside_catalog, "TITLE")
        title.text = cat.title
        artist = SubElement(cd_inside_catalog, "ARTIST")
        artist.text = cat.artist
        country = SubElement(cd_inside_catalog, "COUNTRY")
        country.text = cat.country
        company = SubElement(cd_inside_catalog, "COMPANY")
        price = SubElement(cd_inside_catalog, "PRICE")
        price.text = str(cat.price)
        year = SubElement(cd_inside_catalog, "YEAR")
        year.text = str(cat.year)

    with open(output, "wb") as res_xml:
        res_xml.write(etree.tostring(catalog_element, pretty_print=True))


if __name__ == "__main__":
    serialize_catalogue()