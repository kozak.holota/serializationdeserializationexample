import click
import yaml

from data_models.students_model import students


@click.command()
@click.option("--output", prompt="Write the path to serialize", help="Path to serialize")
def serialize_students(output):
    with open(output, "w") as next_yaml:
        yaml.dump(students, next_yaml)


if __name__ == "__main__":
    serialize_students()
