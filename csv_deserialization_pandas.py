import os

import pandas

from data_models.address_model import AddressModel


def deserialize_addresses(path_to_csv: str):
    items = pandas.read_csv(path_to_csv).itertuples(index=False, name=None)
    return [AddressModel(*item) for item in items]


if __name__ == "__main__":
    items = deserialize_addresses(os.path.join("files", "addresses.csv"))

    for address in items:
        print(address)
