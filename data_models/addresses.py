from data_models.address_model import AddressModel

addresses = [
    AddressModel(
        name="Ivan",
        surname="Kobyla",
        state="OH",
        city="Illynoys",
        address="Chestnut street 3",
        zip_code=89098
    ),
    AddressModel(
        name="Hanna",
        surname="Oyskin",
        state="NY",
        city="New York",
        address="Burbon lane 45",
        zip_code=54445
    ),
    AddressModel(
        name="Gennadiy",
        surname="Crocodile",
        state="PE",
        city="Philadelphia",
        address="Casco street 2",
        zip_code=38809
    ),
    AddressModel(
        name="Iryna",
        surname="KJoronenko",
        state="PE",
        city="Philadelphia",
        address="Casco street 21",
        zip_code=38811
    )
]
