from io import StringIO


class AddressModel:
    def __init__(self, name, surname, address, city, state, zip_code):
        self.zip_code = zip_code
        self.city = city
        self.state = state
        self.address = address
        self.surname = surname
        self.name = name

    def __str__(self):
        address_stream = StringIO()
        address_stream.write("="*30 + "\n")
        address_stream.write(f"First Name: {self.name}\n")
        address_stream.write(f"Surname: {self.surname}\n")
        address_stream.write(f"ZIP Code: {self.zip_code}\n")
        address_stream.write(f"State: {self.state}\n")
        address_stream.write(f"City: {self.city}\n")
        address_stream.write(f"Address: {self.address}\n")
        address_stream.write("=" * 30 + "\n")

        return address_stream.getvalue()