import os

from lxml import etree
from lxml.etree import ElementTree

from data_models.music_store_models import Catalog, Cd


def deserialize_cd_catalog(path_to_xml: str):
    parsed_xml: ElementTree = None

    with open(path_to_xml) as _xml:
        parsed_xml = etree.parse(_xml)

    catalog = Catalog()

    dom_cds = parsed_xml.xpath("//CD")

    for _cd in dom_cds:
        title = _cd.xpath(".//TITLE")[0].text
        artist = _cd.xpath(".//ARTIST")[0].text
        company = _cd.xpath(".//COMPANY")[0].text
        country = _cd.xpath(".//COUNTRY")[0].text
        price = float(_cd.xpath(".//PRICE")[0].text)
        year = int(_cd.xpath(".//YEAR")[0].text)

        catalog.add_cd(
            Cd(
                title,
                artist,
                country,
                company,
                price,
                year
            )
        )

    return catalog


if __name__ == "__main__":
    catalog = deserialize_cd_catalog(os.path.join("files", "MusicStore.xml"))
    print(catalog)
