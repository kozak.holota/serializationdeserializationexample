import json

import click

from data_models.students_model import students


@click.command()
@click.option("--output", prompt="Write the path to serialize", help="Path to serialize")
def serialize_students(output):
    with open(output, "w") as next_json:
        json.dump(students, next_json)


if __name__ == "__main__":
    serialize_students()
