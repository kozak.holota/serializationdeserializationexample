import csv

import click

from data_models.addresses import addresses


@click.command()
@click.option("--output", prompt="Write the path to serialize", help="Path to serialize")
def serialize_addresses(output):
    with open(output, "w") as csv_writer_fp:
        csv_writer = csv.writer(csv_writer_fp, delimiter=',', quotechar='"')
        for item in addresses:
            csv_writer.writerow([item.name, item.surname, item.address, item.state, item.city, item.zip_code])


if __name__ == "__main__":
    serialize_addresses()