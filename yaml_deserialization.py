import os
from pprint import pprint

import yaml


conf_dict = None

with open(os.path.join("files", "cloud_config.yml")) as yaml_fp:
    conf_dict = yaml.load(yaml_fp, Loader=yaml.FullLoader)

print(f'YAML deserialized object data type: {type(conf_dict)}')
pprint(conf_dict)