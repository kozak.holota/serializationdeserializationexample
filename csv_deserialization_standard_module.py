import csv
import os

from data_models.address_model import AddressModel


def deserialize_addresses(path_to_csv: str):
    addresses: [AddressModel] = list()
    with open(path_to_csv) as csv_fp:
        items = csv.reader(csv_fp, delimiter=',', quotechar='"')
        for item in items:
            name, surname, address, city, state, zip_code = item
            addresses.append(
                AddressModel(
                    name,
                    surname,
                    address,
                    city,
                    state,
                    zip_code
                )
            )

    return addresses


if __name__ == "__main__":
    items = deserialize_addresses(os.path.join("files", "addresses.csv"))

    for address in items:
        print(address)
