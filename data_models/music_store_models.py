from io import StringIO


class Cd:
    def __init__(self, title, artist, country, company, price, year):
        self.year = year
        self.price = price
        self.company = company
        self.country = country
        self.artist = artist
        self.title = title

    def __str__(self):
        return f"""
        ********CD********
        Title: {self.title}
        Artist: {self.artist}
        Company: {self.company}
        Country: {self.country}
        Artist: {self.artist}
        Year: {str(self.year)}
        Price: {str(self.price)} $
        ******************
        """

class Catalog:
    def __init__(self):
        self.cds: [Cd] = list()

    def add_cd(self, cd: Cd):
        self.cds.append(cd)

    def __str__(self):
        str_io = StringIO()
        str_io.write("===========CD's Catalog===========\n")
        for cd in self.cds:
            str_io.write(str(cd))
        str_io.write("================================")

        return str_io.getvalue()